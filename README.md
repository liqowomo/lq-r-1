<h1><code> R1 </code></h1>
<h2><i> Sniffowomo - Rust Learning Repo v1  </i></h2>

----
1. [What ?](#what-)
2. [Directories WTF](#directories-wtf)
3. [Crates Used](#crates-used)

----

# What ? 

1. This repo is for learnin rust from a variety of sources 

# Directories WTF 

> Talking about what the shitty directories are

|        N        |                                          ?                                           |
| :-------------: | :----------------------------------------------------------------------------------: |
| [`col1`](col1/) | Testig out the `colored` crate <br> - works with positional arguments and multi line |
|   [`p1`](p1/)   |                              First test with the colors                              |



# Crates Used 

> Making a list of crates that have been used in this repo

|                     n                      |                                                      ?                                                      |
| :----------------------------------------: | :---------------------------------------------------------------------------------------------------------: |
| [`colored`](https://lib.rs/crates/colored) | **Colored** - Easiest crate you have used for the colors , but this is apparently not recommended for doing |