/* Testing
Colored - https://lib.rs/crates/colored
Simplest way of doing colors
user positional
*/

// Calling Crate
use ::colored::*;

fn main() {
    println!(
        "
    Printing Colored Output 
    =======================
    "
    );
    color_print();
}

fn color_print() {
    // Writing the color function here
    println!(
        "{} ,
        {},
        {},
        {}",
        "
        =======================
        "
        .blue(),
        "Sexy colors text".magenta(),
        "
Multi 
Step 
Text
"
        .green(),
        "RedBlink".red().blink()
    );
}
